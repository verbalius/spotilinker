import http from 'serverless-http';
import { tall } from 'tall'; // unshortens urls
import { Telegraf} from 'telegraf';
import { message } from 'telegraf/filters';
import { getSentByCaption } from './getSentByCaption';
import SpotifyWebApi from 'spotify-web-api-node';

const token = process.env.TELEGRAM_API_KEY!;
const bot = new Telegraf(token);
const spotifyNormalUrlRegex = /https\:\/\/open\.spotify\.com\/(playlist|track|album)\/([a-zA-Z0-9]+)/;
const spotifyShortUrlRegex = /https\:\/\/spotify\.link\/[a-zA-Z0-9]+/;

bot.start((ctx) => ctx.reply('Hi! I am spotilinker bot. Send me a Spotify link and I will reply with a beautiful message with an image and metadata.'));
bot.help((ctx) => ctx.reply('Send me a Spotify link.'));

bot.on(message("text"), async (ctx) => {
    try {
        // console.log(`---\n${Date()}\n---\nRequest from user: ${ctx.message.from.username}`);
        const spotifyShortLink = ctx.message.text.match(spotifyShortUrlRegex)
        let parsed_message = null;
        if (spotifyShortLink !== null) {
            const normalSpotifyUrl = await tall(spotifyShortLink[0])
            parsed_message = normalSpotifyUrl.match(spotifyNormalUrlRegex);
        } else {
            parsed_message = ctx.message.text.match(spotifyNormalUrlRegex);
        }
        if (parsed_message === null) {
            return;
        }
        const media_type = parsed_message[1];
        const media_id = parsed_message[2];
        let spotifyApiResponse = null;

        // TODO: add youtube and apple music support
        // const SpotifyToYoutube = require('spotify-to-youtube')
        const spotifyApi = new SpotifyWebApi({
            clientId: process.env.SPOTIFY_CLIENT_ID!,
            clientSecret: process.env.SPOTIFY_CLIENT_SECRET!
        });

        // Get an access token and 'save' it using a setter
        await spotifyApi.clientCredentialsGrant().then(
            function(data) {
                spotifyApi.setAccessToken(data.body['access_token']);
                console.log('Configured Spotify API')
            },
            function(err) {
                console.error('Something went wrong!', err);
            }
        );

        let media_author: string = '';
        let media_image_url: string = '';
        let media_preview_url: string = '';
        let is_playlist: boolean = false;
        const sentByCaption = getSentByCaption(ctx.message.from)

        if (media_type === 'playlist') {
            spotifyApiResponse = await spotifyApi.getPlaylist(media_id);
            media_author = spotifyApiResponse.body.owner.display_name || '';
            media_image_url = spotifyApiResponse.body.images[1].url;
            is_playlist = true;
        }
        else if (media_type === 'album') {
            spotifyApiResponse = await spotifyApi.getAlbum(media_id);
            media_author = spotifyApiResponse.body.artists[0].name;
            media_image_url = spotifyApiResponse.body.images[1].url;
        }
        else if (media_type === 'track') {
            spotifyApiResponse = await spotifyApi.getTrack(media_id);
            media_author = spotifyApiResponse.body.artists[0].name;
            media_image_url = spotifyApiResponse.body.album.images[1].url;
            media_preview_url = spotifyApiResponse.body.preview_url || '';
        } else {
            throw new Error("Media type is incorrect. Exiting.")
        }

        const media_name: string = spotifyApiResponse.body.name;
        const media_metadata: string = `${media_name} - ${media_type} by ${media_author}`;
        const media_link: string = spotifyApiResponse.body.external_urls.spotify;
        const media_image = {
            url: media_image_url,
            filename: spotifyApiResponse.body.external_urls.spotify
        };
        if (media_preview_url !== '') {
            const yt_link: string = "https://music.youtube.com/search?q="+encodeURIComponent(media_author+" "+media_name);
            const apple_link: string = "https://music.apple.com/us/search?term="+encodeURIComponent(media_author+" "+media_name);
            const with_preview_audio_extras = {
                parse_mode: 'HTML',
                caption: `${media_metadata}${sentByCaption}`,
                reply_markup: {
                    "inline_keyboard": [
                        [
                            { "text": '🎧 Spotify', "url": media_link, "hide": false },
                            { "text": '🔎 YTMusic', "url": yt_link, "hide": is_playlist },
                            { "text": '🔎 Apple', "url": apple_link, "hide": is_playlist }
                        ]
                    ]
                },
            }
            // @ts-ignore
            await ctx.replyWithPhoto(media_image, with_preview_audio_extras);
            await ctx.replyWithAudio(media_preview_url, {caption: `Preview: ${media_name} - ${media_type} by ${media_author}`});
        } else {
            const only_photo_extras = {
                parse_mode: 'HTML',
                caption: `${media_metadata}${sentByCaption}`,
                reply_markup: {
                    "inline_keyboard": [
                        [
                            { "text": '💿 Spotify', "url": media_link, "hide": false }
                        ]
                    ]
                },
            };

            // @ts-ignore
            ctx.replyWithPhoto(media_image, only_photo_extras);
        }
        // deleting the original message in groups requires admin
        try {
            ctx.telegram.deleteMessage(ctx.chat.id, ctx.message.message_id);
        } catch (error) {
            ctx.reply('Failed to remove original message.. To let me remove original message, please make me as an admin in this group.');
        }
    } catch (error) {
        try {
            ctx.reply('Oops, sorry there was an error..');
            // @ts-ignore
            ctx.sendMessage(process.env.DEBUG_TG_CHAT_ID!, error); // send it to chat with me
            console.log(error);
        } catch (error) {
            console.log(error);
        }
    }
});

// setup webhook
const spotilinker = http(bot.webhookCallback("/spotilinker"));

export {bot,spotilinker}
