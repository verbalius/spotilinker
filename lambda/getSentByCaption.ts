import {User} from 'telegraf/types';

export const getSentByCaption = (from: User): string => {
  if (from.username) {
    return `\n\nSent by: @${from.username}`
  }

  let fullName = ''
  const firstName = from.first_name
  const lastName = from.last_name
  const userId = from.id

  if (firstName) {
    fullName += firstName + ' '
  }
  if (lastName) {
    fullName += lastName
  }
  if (!fullName) {
    return ''
  }
  return `\n\nSent by: <a href="tg://user?id=${userId}">${fullName}</a>`
}