# spotilinker telegram bot
Telegram bot that gets spotify link and generates message with picture and link button.

## Usage
1. Add it! [t.me/spotilinker_bot](https://t.me/spotilinker_bot).
2. Send spotify link e.g. https://open.spotify.com/track/6ANo1GJcsoNaqct0NOTCs1
3. Get nice reply

Example reply:

## Libs
- Telegram API: [telegraf](https://github.com/telegraf/telegraf)