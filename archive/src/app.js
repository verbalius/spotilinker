require('dotenv').config();
const { Telegraf } = require('telegraf');
const SpotifyWebApi = require('spotify-web-api-node');
// TODO: add youtube and apple music support
// const SpotifyToYoutube = require('spotify-to-youtube')

const spotifyApi = new SpotifyWebApi({
    clientId: process.env.SPOTIFY_CLIENT_ID,
    clientSecret: process.env.SPOTIFY_CLIENT_SECRET
});

const bot = new Telegraf(process.env.TELEGRAM_API_KEY);
const spotifyRegex = /https\:\/\/open\.spotify\.com\/(playlist|track|album)\/(.+)/

bot.start((ctx) => ctx.reply('Hi! I am spotilinker bot. Send me a Spotify link and I will reply with a beautiful message with an image and metadata.'));
bot.help((ctx) => ctx.reply('Send me a Spotify link.'));

bot.hears(spotifyRegex, async (ctx) => {
    try {
        // console.log(`---\n${Date()}\n---\nRequest from user: ${ctx.message.from.username}`);
        var parsed_message = ctx.message.text.match(spotifyRegex);
        var media_type = parsed_message[1];
        var media_id = parsed_message[2];
        var spotifyApiResponse = '';

        const spotifyApi = new SpotifyWebApi({
            clientId: process.env.SPOTIFY_CLIENT_ID,
            clientSecret: process.env.SPOTIFY_CLIENT_SECRET
        });
        
        const initSpotifyCreds = async () => {
            spotifyApi.setAccessToken((await spotifyApi.clientCredentialsGrant()).body['access_token']);
        }

        try {
            await spotifyApi.getMe(); // try fetching test data
        } catch {
            await initSpotifyCreds();
        }

        var media_preview_url = ''
        var is_playlist = false

        if (media_type == 'playlist') {
            spotifyApiResponse = await spotifyApi.getPlaylist(media_id);
            var media_author = spotifyApiResponse.body.owner.display_name;
            var media_image_url = spotifyApiResponse.body.images[1].url;
            is_playlist = true;
        }
        else if (media_type == 'album') {
            spotifyApiResponse = await spotifyApi.getAlbum(media_id);
            var media_author = spotifyApiResponse.body.artists[0].name;
            var media_image_url = spotifyApiResponse.body.images[1].url;
        }
        else if (media_type == 'track') {
            spotifyApiResponse = await spotifyApi.getTrack(media_id);
            var media_author = spotifyApiResponse.body.artists[0].name;
            var media_image_url = spotifyApiResponse.body.album.images[1].url;
            var media_preview_url = spotifyApiResponse.body.preview_url;
        }

        var media_name = spotifyApiResponse.body.name;
        var media_metadata = `${media_name} - ${media_type} by ${media_author}`;
        var media_link = spotifyApiResponse.body.external_urls.spotify;
        var media_image = {
            url: media_image_url,
            filename: spotifyApiResponse.body.external_urls.spotify
        };
        if (media_preview_url != '') {
            var yt_link = "https://music.youtube.com/search?q="+encodeURIComponent(media_author+" "+media_name);
            var apple_link = "https://music.apple.com/us/search?term="+encodeURIComponent(media_author+" "+media_name);
            const with_preview_audio_extras = {
                caption: `${media_metadata}\n\nSent by: @${ctx.message.from.username}`,
                reply_markup: {
                    "inline_keyboard": [
                        [
                            { "text": '🎧 Spotify', "url": media_link, "hide": false },
                            { "text": '🔎 YTMusic', "url": yt_link, "hide": is_playlist },
                            { "text": '🔎 Apple', "url": apple_link, "hide": is_playlist }
                        ]
                    ]
                },
            }
            await ctx.replyWithPhoto(media_image, with_preview_audio_extras);
            await ctx.replyWithAudio(media_preview_url, {caption: `Preview: ${media_name} - ${media_type} by ${media_author}`});
        } else {
            const only_photo_extras = {
                caption: `${media_metadata}\n\nSent by: @${ctx.message.from.username}`,
                reply_markup: {
                    "inline_keyboard": [
                        [
                            { "text": '💿 Spotify', "url": media_link, "hide": false }
                        ]
                    ]
                },
            };
            ctx.replyWithPhoto(media_image, only_photo_extras);
        }
        // deleting the original message in groups requires admin
        try {
            ctx.tg.deleteMessage(ctx.chat.id, ctx.message.message_id);
        } catch (error) {
            ctx.reply('Failed to remove original message.. To let me remove original message, please make me as an admin in this group.');
        }
    } catch (error) {
        try {
            ctx.reply('Oops, sorry there was an error..');
            ctx.sendMessage(process.env.DEBUG_TG_CHAT_ID, error); // send it to chat with me
            console.log(error);
        } catch (error) {
            console.log(error);
        }
    }
});

// initSpotifyCreds();
bot.launch();
console.log('spotilinker [BOT]');