#!/bin/bash
set -e
AWS_PROFILE=personal
AWS_REGION=eu-central-1

aws --profile ${AWS_PROFILE} ecr get-login-password --region "${AWS_REGION}" | docker login --username AWS --password-stdin 081507745108.dkr.ecr.eu-central-1.amazonaws.com
docker build -t 081507745108.dkr.ecr.eu-central-1.amazonaws.com/spotilinker:latest .
docker push 081507745108.dkr.ecr.eu-central-1.amazonaws.com/spotilinker:latest

echo "Deploying"
aws \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    ecs \
    update-service \
    --force-new-deployment \
    --cluster tg-bots \
    --service spotilinker > /dev/null

echo "Waiting.."
aws \
    --profile "${AWS_PROFILE}" \
    --region "${AWS_REGION}" \
    ecs \
    wait services-stable \
    --cluster tg-bots \
    --services spotilinker

[[ $? == 0 ]] && echo "Success!" 